# README

This is a test  for a Wealth management app.
It will allow to list all asset and allow to update value to provide an overview of global Health.
In a second stage it will try to connect to financial institution for automatic update.
It should cover:
- Stock
- Bond
- Fund
- Cryptocurrency
- Cash account
- Saving account
- Credit
- Retirement plan

It will be web base for first version, then a mobile app will be added.
